"""
ObsModDataset : extend xarray's Dataset with methods to manipulate observed (x, y_obs) 
and modeled (x, y_mod) data.
"""

import matplotlib.pyplot as plt
import numpy as np
import plotly.graph_objects as go
import xarray as xr
import xskillscore as xs


@xr.register_dataset_accessor("om_xy")
class XYDataset:
    """ Extend xarray's Dataset to deal with (x, y) observed and modeled data.

    The xarray.Dataset to extend must contain the followings coordinates and data 
    variables.

    :Data coordinates:
    x : xarray.Dataset.coords
        X vector values. Usually time vector data.
    
    :Data variables:
    y_obs : xarray.Dataset.variables
        Y vector observed values.
    y_mod : xarray.Dataset.variables
        Y vector modeled values.
   
    Example
    -------
    .. jupyter-execute::

        import numpy as np
        import pyobsmod
        import pyobsmod.create.xy

        x = np.arange(0, 500, 0.5)
        y_obs = np.sin(x)
        y_mod = np.sin(x) + 0.1*np.cos(x) + 0.05
        ds = pyobsmod.create.xy.from_numpy(x, y_obs, y_mod)
        print(ds.om_xy)
    """

    def __init__(self, xdataset_obj):
        self._obj = xdataset_obj

    def bias(self):
        """ The bias.
        
        Returns
        -------
        bias : xarray.DataArray
            The bias.
        
        Example
        -------
        .. jupyter-execute::

            import numpy as np
            import pyobsmod
            import pyobsmod.create.xy

            x = np.arange(0, 500, 0.5)
            y_obs = np.sin(x)
            y_mod = np.sin(x) + 0.1*np.cos(x) + 0.05
            ds = pyobsmod.create.xy.from_numpy(x, y_obs, y_mod)
            bias = ds.om_xy.bias()
            print(bias)
        """
        
        bias = np.nanmean(self._obj.y_mod-self._obj.y_obs)
        
        return bias

    def lr(self):
        """ Perform linear regression (y=ax+b) using the least squares polynomial fit for 
        degree 1.

        Returns
        -------
        a : float
            The polynomial coefficient of degree one.
        b : float
            The rest.
        
        Example
        -------
        .. jupyter-execute::

            import numpy as np
            import pyobsmod
            import pyobsmod.create.xy

            x = np.arange(0, 500, 0.5)
            y_obs = np.sin(x)
            y_mod = np.sin(x) + 0.1*np.cos(x) + 0.05
            ds = pyobsmod.create.xy.from_numpy(x, y_obs, y_mod)
            a, b = ds.om_xy.lr()
            print(a)
            print(b)
        """
        
        # linear regression
        a, b = np.polyfit(self._obj.y_obs, self._obj.y_mod, deg=1)

        return a, b

    def nrmse(self, norm="mean"):
        """ The normalized root mean squared error.
        
        Parameters
        ----------
        norm : str
            The method to normalized the rmse.
            "mean" : divide by the mean of the observed data.
            "absolute difference" : divide by abs(max(y_obs)-min(y_obs)).

        Returns
        -------
        nrmse : xarray.DataArray
            The normalized root mean squared error.
        
        Example
        -------
        .. jupyter-execute::

            import numpy as np
            import pyobsmod
            import pyobsmod.create.xy

            x = np.arange(0, 500, 0.5)
            y_obs = np.sin(x)
            y_mod = np.sin(x) + 0.1*np.cos(x) + 0.05
            ds = pyobsmod.create.xy.from_numpy(x, y_obs, y_mod)
            nrmse = ds.om_xy.nrmse(norm="mean")
            print(nrmse)
            nrmse = ds.om_xy.nrmse(norm="absolute difference")
            print(nrmse)
        """

        rmse = self.rmse()
        # normalize
        nrmse = []
        if norm == "mean":
            nrmse = rmse/np.nanmean(self._obj.y_obs)
        elif norm == "absolute difference":
            nrmse = rmse/(np.abs(np.nanmax(self._obj.y_obs)-np.nanmin(self._obj.y_obs)))            

        return nrmse

    def rmse(self):
        """ The root mean squared error.
        
        Returns
        -------
        rmse : xarray.DataArray
            The root mean squared error.
        
        Example
        -------
        .. jupyter-execute::

            import numpy as np
            import pyobsmod
            import pyobsmod.create.xy

            x = np.arange(0, 500, 0.5)
            y_obs = np.sin(x)
            y_mod = np.sin(x) + 0.1*np.cos(x) + 0.05
            ds = pyobsmod.create.xy.from_numpy(x, y_obs, y_mod)
            rmse = ds.om_xy.rmse()
            print(rmse)
        """

        rmse = xs.rmse(self._obj.y_obs, self._obj.y_mod, skipna=True)
        
        return rmse

    def r(self):
        """ The Pearson’s correlation coefficient. Correlation between sets of data is a 
        measure of how well they are related. The most common measure of correlation in stats 
        is the Pearson Correlation. It shows the linear relationship between two sets of data. 
        In simple terms, it answers the question, Can I draw a line graph to represent the data?

        Returns
        -------
        r : xarray.DataArray
            The Person's correlation coefficient.
        
        Example
        -------
        .. jupyter-execute::

            import numpy as np
            import pyobsmod
            import pyobsmod.create.xy

            x = np.arange(0, 500, 0.5)
            y_obs = np.sin(x)
            y_mod = np.sin(x) + 0.1*np.cos(x) + 0.05
            ds = pyobsmod.create.xy.from_numpy(x, y_obs, y_mod)
            r = ds.om_xy.r()
            print(r)
        """
        
        # compute pearson correlation coefficient
        r = xs.pearson_r(self._obj.y_obs, self._obj.y_mod, skipna=True)

        return r

    def r2(self):
        """ The Pearson’s correlation coefficient squared.
        
        Returns
        -------
        r2 : xarray.DataArray
            The Person's correlation coefficient squared.
        
        Example
        -------
        .. jupyter-execute::

            import numpy as np
            import pyobsmod
            import pyobsmod.create.xy

            x = np.arange(0, 500, 0.5)
            y_obs = np.sin(x)
            y_mod = np.sin(x) + 0.1*np.cos(x) + 0.05
            ds = pyobsmod.create.xy.from_numpy(x, y_obs, y_mod)
            r2 = ds.om_xy.r2()
            print(r2)
        """
        
        pearson_r = xs.pearson_r(self._obj.y_obs, self._obj.y_mod, skipna=True)
        r2 = pearson_r * pearson_r
        
        return r2

    def plot_all(self, norm="mean", decimals=3):
        """ Plot the data and all indicators.

        Parameters
        ----------
        norm : str
            The method to normalized the rmse.
            "mean" : divide by the mean of the observed data.
            "absolute difference" : divide by abs(max(y_obs)-min(y_obs)).
            The default value is "mean"
        decimals : int
            Number of significant digits to display. The default value is 3.
        
        Example
        -------
        .. jupyter-execute::

            import numpy as np
            import pyobsmod
            import pyobsmod.create.xy

            x = np.arange(0, 500, 0.5)
            y_obs = np.sin(x)
            y_mod = np.sin(x) + 0.1*np.cos(x) + 0.05
            ds = pyobsmod.create.xy.from_numpy(x, y_obs, y_mod)
            ds.om_xy.plot_all(norm="absolute difference")
        """

        # figure
        plt.figure()
        # data
        plt.plot(self._obj.y_obs, self._obj.y_mod, '.')
        # y=x
        plt.axline(xy1=(0, 0), slope=1, color ='k', label="y=x")
        # linear regression
        a, b = self.lr()
        plt.axline(xy1=(0, b), slope=a, label=f'$y = {a:.2f}x {b:+.2f}$', c="r")       
        # errors
        rmse = self.rmse().data
        rmse = np.round(rmse, decimals=decimals)
        nrmse = self.nrmse(norm=norm).data
        nrmse = np.round(nrmse, decimals=decimals)
        # correlation
        r = self.r().data
        r = np.round(r, decimals=decimals)
        r2 = self.r2().data
        r2 = np.round(r2, decimals=decimals)
        # bias
        bias = self.bias()
        bias = np.round(bias, decimals=decimals)
        # linear regression
        a = np.round(a, decimals=decimals)
        b = np.round(b, decimals=decimals)
        # title
        plt.title(f"Errors: rmse={rmse}, nrmse={nrmse}\nCorrelation: r={r}, r²={r2}\nBias: bias={bias}\nLinear regression: a={a}, b={b}")
        # labels
        plt.xlabel("Observed data")
        plt.ylabel("Modeled data")
        # legend
        plt.legend()

    def plot_data(self):
        """ Plot the data.
        
        Example
        -------
        .. jupyter-execute::

            import numpy as np
            import pyobsmod
            import pyobsmod.create.xy

            x = np.arange(0, 500, 0.5)
            y_obs = np.sin(x)
            y_mod = np.sin(x) + 0.1*np.cos(x) + 0.05
            ds = pyobsmod.create.xy.from_numpy(x, y_obs, y_mod)
            ds.om_xy.plot_data()
        """

        # figure
        plt.figure()
        # observed data
        plt.plot(self._obj.x, self._obj.y_obs, '-', label="Observed data")
        # modeled data
        plt.plot(self._obj.x, self._obj.y_mod, '-', label="Modeled data", color='r')
        # labels
        plt.xlabel("X")
        plt.ylabel("Y")
        # legend
        plt.legend(bbox_to_anchor=(1.05, 1.0), loc='upper left')

    def plotly_all(self, norm="mean", decimals=3):
        """ Plot the data and all indicators with plotly.
                
        Parameters
        ----------
        norm : str
            The method to normalized the rmse.
            "mean" : divide by the mean of the observed data.
            "absolute difference" : divide by abs(max(y_obs)-min(y_obs)).
            The default value is "mean"
        decimals : int
            Number of significant digits to display. The default value is 3.
        
        Results
        -------
        fig : plotly.graph_objects.Figure
            A plotly graph_objects of the correlation graph.
        
        Example
        -------
        .. jupyter-execute::

            import numpy as np
            import pyobsmod
            import pyobsmod.create.xy

            x = np.arange(0, 500, 0.5)
            y_obs = np.sin(x)
            y_mod = np.sin(x) + 0.1*np.cos(x) + 0.05
            ds = pyobsmod.create.xy.from_numpy(x, y_obs, y_mod)
            ds.om_xy.plotly_all(norm="absolute difference")
        """

        # linear regression
        a, b = self.lr()
        # errors
        rmse = self.rmse().data
        rmse = np.round(rmse, decimals=decimals)
        nrmse = self.nrmse(norm=norm).data
        nrmse = np.round(nrmse, decimals=decimals)
        # correlation
        r = self.r().data
        r = np.round(r, decimals=decimals)
        r2 = self.r2().data
        r2 = np.round(r2, decimals=decimals)
        # bias
        bias = self.bias()
        bias = np.round(bias, decimals=decimals)
        # linear regression
        a = np.round(a, decimals=decimals)
        b = np.round(b, decimals=decimals)
        
        # figure
        fig = go.Figure()
        # data
        fig.add_trace(go.Scatter(x=self._obj.y_obs, y=self._obj.y_mod,
            mode='markers',
            name='Data'))
        # y=x
        fig.add_shape(type='line',
                    x0=np.min(self._obj.y_obs.data),
                    y0=np.min(self._obj.y_obs.data),
                    x1=np.max(self._obj.y_obs.data),
                    y1=np.max(self._obj.y_obs.data),
                    line=dict(color='Black',),
                    xref='x',
                    yref='y',
                    name="y=x",
        )
        # linear regression
        fig.add_shape(type='line',
                    x0=np.min(self._obj.y_obs.data),
                    y0=a*np.min(self._obj.y_obs.data)+b,
                    x1=np.max(self._obj.y_obs.data),
                    y1=a*np.max(self._obj.y_obs.data)+b,
                    line=dict(color='Red',),
                    xref='x',
                    yref='y'
        )
        # layout
        fig.update_layout(
            title=f"Errors: rmse={rmse}, nrmse={nrmse}; Correlation: r={r}, r²={r2}<br>Bias: bias={bias}; Linear regression: a={a}, b={b}",
            xaxis_title='Observed data',
            yaxis_title='Modeled data',
            hovermode="x unified",
            #height=800,
            #width=1000,
            )
        # axes
        fig.update_xaxes(showspikes=True, spikesnap="cursor", spikemode="across")
        fig.update_yaxes(showspikes=True, spikethickness=2)

        return fig
   
    def plotly_data(self, mode="lines"):
        """ Plot the data with plotly.
        
        Parameters
        ----------
        mode : str, {lines, markers}
            The mode of the scatter plot for plotly.
            "lines" : line plot.
            "markers" : marker plot.
            The default value is "lines".

        Returns
        -------
        fig : plotly.graph_objects.Figure
            A plotly graph_objects of the data.
        
        Example
        -------
        .. jupyter-execute::

            import numpy as np
            import pyobsmod
            import pyobsmod.create.xy

            x = np.arange(0, 500, 0.5)
            y_obs = np.sin(x)
            y_mod = np.sin(x) + 0.1*np.cos(x) + 0.05
            ds = pyobsmod.create.xy.from_numpy(x, y_obs, y_mod)
            ds.om_xy.plotly_data()
        """

        # figure
        fig = go.Figure()
        # observed data
        fig.add_trace(go.Scatter(x=self._obj.x, y=self._obj.y_obs,
            mode=mode,
            name='Observed data'))
        # modeled data
        fig.add_trace(go.Scatter(x=self._obj.x, y=self._obj.y_mod,
            mode=mode,
            name='Modeled data'))
        # layout
        fig.update_layout(
            xaxis_title='X',
            yaxis_title='Y',
            hovermode="x unified")
        
        return fig
