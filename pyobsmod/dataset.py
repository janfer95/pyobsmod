"""
Dataset
-------

Dataset is a class built on pandas DataFrame that allows for quick plotting
and analyzing of observational vs modelled predicted data.
"""

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import warnings

from matplotlib.offsetbox import AnchoredText
from typing import Any, Literal, Optional
from pathlib import Path


class Dataset:
    """Dataset is a class built on pandas DataFrames that allows for quick
    plotting and analyzing of observational vs modelled predicted data.

    Parameters
    ----------
    obs : Optional[np.ndarray]
        Observation data. Should preferably be a 1-dimensional np.ndarray, but
        lists and tuples can be passed as well
    pred : Optional[np.ndarray]
        Predicted / modelled data. Should preferably be a 1-dimensional
        np.ndarray, but lists and tuples can be passed as well
    df : Optional[pd.DataFrame]
        Instead of passing two numpy arrays, it is also possible to pass
        directly a DataFrame that must contain at least the columns ``obs``
        and ``pred``. Note also that if ``df`` is ``None``, both ``obs`` and
        ``pred`` must not be ``None``
    time : Optional[np.ndarray | pd.Index | pd.DatetimeIndex]
        A 1-dimensional array with the time steps of the observation and
        prediction data. Will be used automatically as ticks in certain plots
        like ``Dataset.time_series_plot``

    Example
    -------
    Create a dataset from some data.

    .. jupyter-execute::

        import numpy as np
        import pyobsmod as pom

        obs = np.sin(np.arange(100)) + np.random.normal(size=100)
        pred = np.sin(np.arange(100)) + np.random.normal(size=100)
        op = pom.Dataset(obs, pred)
        print(op)
    """

    def __init__(
        self,
        obs: Optional[np.ndarray] = None,
        pred: Optional[np.ndarray] = None,
        df: Optional[pd.DataFrame] = None,
        time: Optional[np.ndarray | pd.Index | pd.DatetimeIndex] = None,
    ) -> None:
        if df is not None:
            self.df = df
        elif obs is not None and pred is not None:
            self.df = pd.DataFrame(dict(obs=obs, pred=pred))
        else:
            raise ValueError(
                    "Either both obs and pred, or df must be not None."
                )

        if time is not None:
            self.df = self.df.set_index(time)

    def __repr__(self) -> str:
        return "pyobsmod.Dataset(\n" + self.df.__repr__() + "\n)"

    def __len__(self):
        return len(self.df)

    def __contains__(self, item):
        if not isinstance(item, str):
            raise ValueError(
                f"{item} must be a column of the underlying dataframe"
            )
        return (item in self.df.columns)

    def __getitem__(self, position):
        if not isinstance(position, slice):
            position = [position]
        return Dataset(df=self.df.iloc[position])

    def __getattr__(self, name):
        # __getattr__ is only called if no class attributes match 'name'
        if name in self:
            return self.df[name].values

        # Fall back to default behaviour (and error) otherwise
        return super().__getattribute__(name)

    def __setattr__(self, name, value):
        # Allows users to set obs and pred without accessing the dataframe
        if name in {"obs", "pred"}:
            setattr(self.df, name, value)

        # Fall back to default behaviour (and error) otherwise
        super().__setattr__(name, value)

    @property
    def df(self):
        return self._df

    @df.setter
    def df(self, new_df):
        # Check if object is a dataframe with (at least) columns obs and pred
        if not isinstance(new_df, pd.DataFrame):
            raise TypeError("``new_df`` must be a pandas DataFrame.")
        if not {"obs", "pred"}.issubset(new_df.columns):
            raise ValueError(
                    "Dataframe must contain columns ``obs`` " "and ``pred``."
                )

        self._df = new_df

    @property
    def values(self):
        """Quick access to the dataset as a numpy array"""
        return self._df.values

    @property
    def data(self):
        """Alias for ``self.values``"""
        return self.values

    def save(self, path: str | Path) -> None:
        """Save this class as a pickle file

        Internally only the pandas dataframe is saved and the class can
        be reconstructed from that dataframe. If needed then, the pickle
        file can be loaded directly in pandas with ``pd.read_pickle``.

        Parameters
        ----------
        path : str | Path
            File path where the pickled object will be stored
        """
        self.df.to_pickle(path)

    def bias(self) -> float:
        """The bias.

        Return
        ------
        bias : float
            The bias.

        Example
        -------
        .. jupyter-execute::

            import numpy as np
            import pyobsmod as pom

            obs = np.sin(np.arange(100)) + np.random.normal(size=100)
            pred = np.sin(np.arange(100)) + np.random.normal(size=100)
            op = pom.Dataset(obs, pred)
            print(op.bias())
        """
        return float((self.df.pred - self.df.obs).mean())

    def lr(self) -> tuple[float, float]:
        """Perform linear regression (y=ax+b) using a least squares polynomial
        fit of degree 1.

        Returns
        -------
        lr : list[float]
            A list with two floats, the slope a and the intercept b.

        Example
        -------
        .. jupyter-execute::

            import numpy as np
            import pyobsmod as pom

            obs = np.sin(np.arange(100)) + np.random.normal(size=100)
            pred = np.sin(np.arange(100)) + np.random.normal(size=100)
            op = pom.Dataset(obs, pred)
            print(op.lr())
        """
        a, b = np.polyfit(self.obs, self.pred, deg=1)

        return (a, b)

    def rmse(self) -> float:
        """The root mean squared error.

        Return
        ------
        rmse : float
            The root mean squared error.

        Example
        -------
        .. jupyter-execute::

            import numpy as np
            import pyobsmod as pom

            obs = np.sin(np.arange(100)) + np.random.normal(size=100)
            pred = np.sin(np.arange(100)) + np.random.normal(size=100)
            op = pom.Dataset(obs, pred)
            print(op.rmse())
        """
        return float(((self.df.obs - self.df.pred) ** 2).mean() ** 0.5)

    def nrmse(
        self,
        norm: Literal["mean", "range"] = "mean",
    ) -> float:
        """The normalized root mean squared error.

        Parameter
        ---------
        norm : Literal['mean', 'range']
            The method to normalize the rmse :

                * mean : divide by the mean of the observed data
                * range : divide by max(y_obs) - min(y_obs)

            The default value is "mean".

        Return
        ------
        nrmse : float
            The normalized root mean squared error.

        Example
        -------
        .. jupyter-execute::

            import numpy as np
            import pyobsmod as pom

            obs = np.sin(np.arange(100)) + np.random.normal(size=100)
            pred = np.sin(np.arange(100)) + np.random.normal(size=100)
            op = pom.Dataset(obs, pred)
            print(op.nrmse())
        """
        if norm == "mean":
            return float(self.rmse() / self.df.obs.mean())
        elif norm == "range":
            return float(self.rmse() / (self.df.obs.max() - self.df.obs.min()))
        else:
            raise ValueError("'norm' must be one of 'mean' or 'range'.")

    def r(
        self,
        method: Literal["pearson", "kendall", "spearman"] = "pearson",
    ) -> float:
        """The correlation coefficient. Correlation between sets of data is a
        measure of how well they are related. The most common measure of
        correlation in stats is the Pearson Correlation. It shows the linear
        relationship between two sets of data. In simple terms, it answers the
        question, Can I draw a line graph to represent the data?

        Parameter
        ---------
        method : str
            Method of correlation:

                * pearson : standard correlation coefficient
                * kendall : Kendall Tau correlation coefficient
                * spearman : Spearman rank correlation

            The default value is 'pearson'.

        Return
        ------
        r : float
            The correlation coefficient.

        Example
        -------
        .. jupyter-execute::

            import numpy as np
            import pyobsmod as pom

            obs = np.sin(np.arange(100)) + np.random.normal(size=100)
            pred = np.sin(np.arange(100)) + np.random.normal(size=100)
            op = pom.Dataset(obs, pred)
            print(op.r())
        """
        return float(self.df.corr(method=method).loc["obs", "pred"])

    def r2(
        self,
        method: Literal["pearson", "kendall", "spearman"] = "pearson",
    ) -> float:
        """The correlation coefficient squared.

        Parameter
        ---------
        method : str
            Method of correlation:

                * pearson : standard correlation coefficient
                * kendall : Kendall Tau correlation coefficient
                * spearman : Spearman rank correlation

            The default value is 'pearson'.

        Return
        ------
        r2 : float
            The correlation coefficient squared.

        Example
        -------
        .. jupyter-execute::

            import numpy as np
            import pyobsmod as pom

            obs = np.sin(np.arange(100)) + np.random.normal(size=100)
            pred = np.sin(np.arange(100)) + np.random.normal(size=100)
            op = pom.Dataset(obs, pred)
            print(op.r2())
        """
        return float(self.r(method) ** 2)

    def compute_stats(
        self,
        stats_list: list[str],
    ) -> pd.Series:
        """The statistics parameters to compute.

        Parameter
        ---------
        stats_list : list[str]
            List of the statistics parameters to compute.

        Return
        ------
        stats : pd.Series
            A list of the statistic parameters.

        Notes
        -----
        For now doesn't allow arguments to be passed.

        Example
        -------
        .. jupyter-execute::

            import numpy as np
            import pyobsmod as pom

            obs = np.sin(np.arange(100)) + np.random.normal(size=100)
            pred = np.sin(np.arange(100)) + np.random.normal(size=100)
            op = pom.Dataset(obs, pred)
            print(op.compute_stats(['rmse', 'nrmse']))
        """

        stats = []
        for stat in stats_list:
            if hasattr(self, stat):
                stats.append(getattr(self, stat)())
            else:
                warnings.warn(
                    f"'{stat}' is not a method of '{self.__class__.__name__}'"
                )
        stats = pd.Series(stats, stats_list)
        return stats

    def describe_dataset(self) -> pd.Series:
        """A utility function that computes the bias, rmse, nrmse, and r2
        of the data set.

        Returns
        -------
        stats : pd.Series
            A pandas series containing the bias, rmse, nrmse, and r2.
        """
        stats_list = ['bias', 'rmse', 'nrmse', 'r2']
        stats = self.compute_stats(stats_list)

        return stats

    def __create_textbox(
        self,
        stats_list: list[str],
        stats_labels: Optional[list[str]] = None,
        textbox_kws: Optional[dict[str, Any]] = None,
    ) -> AnchoredText:
        """Create statistics textbox for plots

        Parameters
        ----------
        stats_list : list[str]
            List of statistics to compute
        stats_labels : Optional[list[str]], optional
            List of labels that are used in the textbox instead of
            ``stats_list``. Has to be in the same order than ``stats_list``
        textbox_kws : Optional[dict[str, Any]], optional
            Dictionary that is passed to matplotlibs' ``AnchoredText``

        Returns
        -------
        anchored_text : AnchoredText
            Textbox with all statistics of ``stats_list``
        """
        # Use stats_list as labels if stats_labels is not passed
        if stats_labels is None:
            stats_labels = stats_list.copy()

        stats = self.compute_stats(stats_list)
        # Display the computed statistics in a text box using AnchoredText
        text_box_strings = []
        for stat_item, stat in zip(stats_labels, stats):
            text_box_strings.append(f"{stat_item}: {stat:.2f}")

        text_box_content = "\n".join(text_box_strings)

        anchored_text = AnchoredText(text_box_content, **textbox_kws)

        return anchored_text

    def scatter_plot(
        self,
        stats_list: Optional[list[str]] = None,
        stats_labels: Optional[list[str]] = None,
        ax: Optional[plt.Axes] = None,
        idline_kws: Optional[dict[str, Any]] = None,
        textbox_kws: Optional[dict[str, Any]] = None,
        **kwargs,
    ) -> plt.Axes:
        """Plot observed data against predicted data with an identity line and
        display selected statistics.

        Parameters
        ----------
        stats_list: Optional[list[str]]
            List of statistis to compute
        stats_labels: Optional[list[str]]
            List of labels that are used in the textbox instead of
            ``stats_list``. Has to be in the same order than ``stats_list``
        ax: Optional[plt.Axes]
            Matplotlib axis to draw the plot on
        idline_kws: Optional[dict[str, Any]]
            Dictionary that is passed to ``plt.axline``
        textbox_kws: Optional[dict[str, Any]]
            Dictionary that is passed to matplotlibs' ``AnchoredText``
        **kwargs:
            Additional arguments that are passed to ``plt.scatter``

        Returns
        -------
        ax : plt.Axes
            The Matplotlib axes.

        Example
        -------
        .. jupyter-execute::

            import matplotlib.pyplot as plt
            import numpy as np
            import pyobsmod as pom

            obs = np.sin(np.arange(100)) + np.random.normal(size=100)
            pred = np.sin(np.arange(100)) + np.random.normal(size=100)
            op = pom.Dataset(obs, pred)
            op.scatter_plot(['bias', 'rmse', 'nrmse', 'r2'])
            plt.show()
        """
        if ax is None:
            ax = plt.gca()

        # Create scatter plot
        ax.scatter(self.obs, self.pred, **kwargs)

        # Set up default arguments that can be overwritten by dictionary
        idline_kws = {} if idline_kws is None else idline_kws.copy()
        idline_kws.setdefault("lw", 1)
        idline_kws.setdefault("color", "red")

        textbox_kws = {} if textbox_kws is None else textbox_kws.copy()
        textbox_kws.setdefault("loc", "upper left")
        textbox_kws.setdefault("frameon", True)
        textbox_kws.setdefault("borderpad", 1.5)

        # Plot identity line
        obsmin = self.df.obs.min()
        obsmax = self.df.obs.max()
        ax.axline((obsmin, obsmin), (obsmax, obsmax), **idline_kws)

        # Set labels and title
        ax.set_title("Observed vs Predicted Data", fontsize=16)
        ax.set_xlabel("Observed Data", fontsize=14)
        ax.set_ylabel("Predicted Data", fontsize=14)

        if stats_list is not None:
            anchored_text = self.__create_textbox(
                stats_list=stats_list,
                stats_labels=stats_labels,
                textbox_kws=textbox_kws,
            )

            ax.add_artist(anchored_text)

        return ax

    def scatter_plot_sns(
        self,
        stats_list: Optional[list[str]] = None,
        stats_labels: Optional[list[str]] = None,
        idline_kws: Optional[dict[str, Any]] = None,
        textbox_kws: Optional[dict[str, Any]] = None,
        **kwargs,
    ) -> sns.JointGrid:
        """Plot observed data against predicted data with an identity line and
        display selected statistics with seaborn's jointplot.

        Parameters
        ----------
        stats_list: Optional[list[str]]
            List of statistis to compute
        stats_labels: Optional[list[str]]
            List of labels that are used in the textbox instead of
            ``stats_list``. Has to be in the same order than ``stats_list``.
        idline_kws: Optional[dict[str, Any]]
            Dictionary that is passed to ``plt.axline``
        textbox_kws: Optional[dict[str, Any]]
            Dictionary that is passed to matplotlibs' ``AnchoredText``
        **kwargs:
            Additional arguments that are passed to ``sns.jointplot``

        Return
        ------
        grid : sns.JointGrid
            Seaborn JointGrid.

        Notes
        -----
        Note that in contrast to ``Dataset.scatter_plot`` and
        ``Dataset.time_series_plot`` this method does NOT take a ``plt.Axes``
        object as an argument, since the underlying function creates
        a figure and several axes objects itself.

        In practice this means simply, that this plot can NOT be used
        in subplots (without major work-arounds).

        Example
        -------
        .. jupyter-execute::

            import matplotlib.pyplot as plt
            import numpy as np
            import pyobsmod as pom

            obs = np.sin(np.arange(100)) + np.random.normal(size=100)
            pred = np.sin(np.arange(100)) + np.random.normal(size=100)
            op = pom.Dataset(obs, pred)
            grid = op.scatter_plot_sns(['bias', 'rmse', 'nrmse', 'r2'])
            # You can (but don't have to) access the figure and axes
            fig = grid.fig
            ax = grid.ax_joint
            plt.show()
        """
        # Set up default arguments that can be overwritten by dictionary
        idline_kws = {} if idline_kws is None else idline_kws.copy()
        idline_kws.setdefault("lw", 1)
        idline_kws.setdefault("color", "black")

        textbox_kws = {} if textbox_kws is None else textbox_kws.copy()
        textbox_kws.setdefault("loc", "upper left")
        textbox_kws.setdefault("frameon", True)
        textbox_kws.setdefault("borderpad", 1.5)

        kwargs.setdefault("kind", "reg")
        kwargs.setdefault("height", 10)
        kwargs.setdefault(
            "joint_kws", dict(line_kws=dict(color="red", label="Regression"))
        )

        # Create scatter plot
        grid = sns.jointplot(
            data=self.df, x="obs", y="pred", label="Predictions", **kwargs
        )
        ax = grid.ax_joint

        # Plot identity line
        obsmin = self.df.obs.min()
        obsmax = self.df.obs.max()
        ax.axline(
            (obsmin, obsmin), (obsmax, obsmax),
            label="Identity line", **idline_kws
        )

        # Set labels and title
        ax.set_title("Observed vs Predicted Data", fontsize=16)
        ax.set_xlabel("Observed Data", fontsize=14)
        ax.set_ylabel("Predicted Data", fontsize=14)

        if stats_list is not None:
            anchored_text = self.__create_textbox(
                stats_list=stats_list,
                stats_labels=stats_labels,
                textbox_kws=textbox_kws,
            )

            ax.add_artist(anchored_text)

        return grid

    def time_series_plot(
        self,
        stats_list: Optional[list[str]] = None,
        stats_labels: Optional[list[str]] = None,
        ax: Optional[plt.Axes] = None,
        textbox_kws: Optional[dict[str, Any]] = None,
        obs_kws: Optional[dict[str, Any]] = None,
        pred_kws: Optional[dict[str, Any]] = None,
        **kwargs,
    ) -> plt.Axes:
        """Plot observed data against predicted data with an identity line and
        display selected statistics.

        Parameters
        ----------
        stats_list: Optional[list[str]]
            List of statistis to compute
        stats_labels: Optional[list[str]]
            List of labels that are used in the textbox instead of
            ``stats_list``. Has to be in the same order than ``stats_list``.
        ax: Optional[plt.Axes]
            Matplotlib axis to draw the plot on
        textbox_kws: Optional[dict[str, Any]]
            Dictionary that is passed to matplotlibs' ``AnchoredText``
        obs_kws: Optional[dict[str, Any]]
            Dictionary that is passed to the observation data ``plt.plot`` call
        pred_kws: Optional[dict[str, Any]]
            Dictionary that is passed to the prediction data ``plt.plot`` call
        **kwargs:
            Additional arguments that are passed to both ``plt.plot`` calls

        Returns
        -------
        ax : plt.Axes
            The Matplotlib axes.

        Example
        -------
        .. jupyter-execute::

            import matplotlib.pyplot as plt
            import numpy as np
            import pyobsmod as pom

            obs = np.sin(np.arange(100)) + np.random.normal(size=100)
            pred = np.sin(np.arange(100)) + np.random.normal(size=100)
            op = pom.Dataset(obs, pred)
            op.time_series_plot(['bias', 'rmse', 'nrmse', 'r2'])
            plt.show()
        """
        if ax is None:
            ax = plt.gca()

        time = self.df.index.values

        # Update obs_ and pred_kws keyword arguments if necessary
        if len(kwargs):
            obs_kws.update(kwargs)
            pred_kws.update(kwargs)

        obs_kws = {} if obs_kws is None else obs_kws.copy()
        obs_kws.setdefault("label", "Observed")

        pred_kws = {} if pred_kws is None else pred_kws.copy()
        pred_kws.setdefault("label", "Predicted")

        textbox_kws = {} if textbox_kws is None else textbox_kws.copy()
        textbox_kws.setdefault("loc", "upper left")
        textbox_kws.setdefault("frameon", True)
        textbox_kws.setdefault("borderpad", 1.5)

        ax.plot(time, self.obs, **obs_kws)
        ax.plot(time, self.pred, **pred_kws)

        # Set labels and title
        ax.set_title("Time Series: Observed vs Predicted", fontsize=16)
        ax.set_xlabel("Time", fontsize=14)
        ax.set_ylabel("Values", fontsize=14)
        ax.legend()

        if stats_list is not None:
            anchored_text = self.__create_textbox(
                stats_list=stats_list,
                stats_labels=stats_labels,
                textbox_kws=textbox_kws,
            )

            ax.add_artist(anchored_text)

        return ax


def load_dataset(path: str | Path) -> Dataset:
    """Function to load ``Dataset`` from a pickled dataframe

    The file to load did not have to be created by ``Dataset.save``
    and can come directly from a saved dataframe ``pd.to_pickle``. However,
    the dataframe still must contain the ``obs`` and ``pred`` columns.

    Parameters
    ----------
    path : str | Path
        File path where the pickled object will be stored

    Returns
    -------
    Dataset
        Dataset with observation and prediction columns
    """
    return Dataset(df=pd.read_pickle(path))
