"""
Plotting module that contains the same plotting functions as defined in
the ``pyobsmod.Dataset`` methods. Functions have the advantage that no
class instance has to be created. However, internally all functions create
a class instance and refer to their respective class methods.
"""

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

from typing import Any, Optional

from pyobsmod.dataset import Dataset


def scatter_plot(
    obs: np.ndarray,
    pred: np.ndarray,
    stats_list: Optional[list[str]] = None,
    stats_labels: Optional[list[str]] = None,
    ax: Optional[plt.Axes] = None,
    idline_kws: Optional[dict[str, Any]] = None,
    textbox_kws: Optional[dict[str, Any]] = None,
    **kwargs,
) -> plt.Axes:
    """Plot observed data against predicted data with an identity line and
    display selected statistics.

    Internally this function create a Dataset object and passes the arguments
    to the ``Dataset.scatter_plot`` method.

    Parameters
    ----------
    obs: np.ndarray
        1d numpy array that contains the data that was observed / measured
    pred: np.ndarray
        1d numpy array that contains the data that was predicted / modelled
    stats_list: Optional[list[str]]
        List of statistis to compute
    stats_labels: Optional[list[str]]
        List of labels that are used in the textbox instead of
        `stats_list`. Has to be in the same order than `stats_list`.
    ax: Optional[plt.Axes]
        Matplotlib axis to draw the plot on
    idline_kws: Optional[dict[str, Any]]
        Dictionary that is passed to ``plt.axline``
    textbox_kws: Optional[dict[str, Any]]
        Dictionary that is passed to matplotlibs' ``AnchoredText``
    **kwargs:
        Additional arguments that are passed to ``plt.scatter``

    Returns
    -------
    ax : plt.Axes
        The Matplotlib axes.

    Example
    -------
    .. jupyter-execute::

        import matplotlib.pyplot as plt
        import numpy as np
        import pyobsmod.plots as pp

        obs = np.sin(np.arange(100)) + np.random.normal(size=100)
        pred = np.sin(np.arange(100)) + np.random.normal(size=100)

        ax = pp.scatter_plot(obs, pred, ['bias', 'rmse', 'nrmse', 'r2'])
        plt.show()
    """
    __checkarrays([obs, pred])
    op = Dataset(obs, pred)
    ax = op.scatter_plot(
        stats_list=stats_list,
        stats_labels=stats_labels,
        ax=ax,
        idline_kws=idline_kws,
        textbox_kws=textbox_kws,
        **kwargs
    )

    return ax


def scatter_plot_sns(
    obs: np.ndarray,
    pred: np.ndarray,
    stats_list: Optional[list[str]] = None,
    stats_labels: Optional[list[str]] = None,
    idline_kws: Optional[dict[str, Any]] = None,
    textbox_kws: Optional[dict[str, Any]] = None,
    **kwargs,
) -> sns.JointGrid:
    """Plot observed data against predicted data with an identity line and
    display selected statistics with seaborn's jointplot.

    Internally this function create a Dataset object and passes the arguments
    to the ``Dataset.scatter_plot_sns`` method.

    Parameters
    ----------
    obs: np.ndarray
        1d numpy array that contains the data that was observed / measured
    pred: np.ndarray
        1d numpy array that contains the data that was predicted / modelled
    stats_list: Optional[list[str]]
        List of statistis to compute
    stats_labels: Optional[list[str]]
        List of labels that are used in the textbox instead of
        ``stats_list``. Has to be in the same order than ``stats_list``.
    idline_kws: Optional[dict[str, Any]]
        Dictionary that is passed to ``plt.axline``
    textbox_kws: Optional[dict[str, Any]]
        Dictionary that is passed to matplotlibs' ``AnchoredText``
    **kwargs:
        Additional arguments that are passed to ``sns.jointplot``

    Return
    ------
    grid : sns.JointGrid
        Seaborn JointGrid.

    Notes
    -----
    Note that in contrast to ``plots.scatter_plot`` and
    ``plots.time_series_plot`` this method does NOT take a ``plt.Axes``
    object as an argument, since the underlying function creates
    a figure and several axes objects itself.

    In practice this means simply, that this plot can NOT be used
    in subplots (without major work-arounds).

    Example
    -------
    .. jupyter-execute::

        import matplotlib.pyplot as plt
        import numpy as np
        import pyobsmod.plots as pp

        obs = np.sin(np.arange(100)) + np.random.normal(size=100)
        pred = np.sin(np.arange(100)) + np.random.normal(size=100)
        g = pp.scatter_plot_sns(obs, pred, ['bias', 'rmse', 'nrmse', 'r2'])
        plt.show()
    """
    __checkarrays([obs, pred])
    op = Dataset(obs, pred)
    grid = op.scatter_plot_sns(
        stats_list=stats_list,
        stats_labels=stats_labels,
        idline_kws=idline_kws,
        textbox_kws=textbox_kws,
        **kwargs
    )

    return grid


def time_series_plot(
    obs: np.ndarray,
    pred: np.ndarray,
    time: Optional[np.ndarray | pd.Index | pd.DatetimeIndex] = None,
    stats_list: Optional[list[str]] = None,
    stats_labels: Optional[list[str]] = None,
    ax: Optional[plt.Axes] = None,
    textbox_kws: Optional[dict[str, Any]] = None,
    obs_kws: Optional[dict[str, Any]] = None,
    pred_kws: Optional[dict[str, Any]] = None,
    **kwargs,
) -> plt.Axes:
    """Plot observed data against predicted data with an identity line and
    display selected statistics.

    Internally this function create a Dataset object and passes the arguments
    to the ``Dataset.scatter_plot`` method.

    Parameters
    ----------
    obs : np.ndarray
        Observation data. Should preferably be a 1-dimensional np.ndarray, but
        lists and tuples can be passed as well
    pred : np.ndarray
        Predicted / modelled data. Should preferably be a 1-dimensional
        np.ndarray, but lists and tuples can be passed as well
    time : Optional[np.ndarray | pd.Index | pd.DatetimeIndex]
        A 1-dimensional array with the time steps of the observation and
        prediction data. Will be used automatically as ticks in certain plots
        like ``Dataset.time_series_plot``
    stats_list: Optional[list[str]]
        List of statistis to compute
    stats_labels: Optional[list[str]]
        List of labels that are used in the textbox instead of
        ``stats_list``. Has to be in the same order than ``stats_list``.
    ax: Optional[plt.Axes]
        Matplotlib axis to draw the plot on
    textbox_kws: Optional[dict[str, Any]]
        Dictionary that is passed to matplotlibs' ``AnchoredText``
    obs_kws: Optional[dict[str, Any]]
        Dictionary that is passed to the observation data ``plt.plot`` call
    pred_kws: Optional[dict[str, Any]]
        Dictionary that is passed to the prediction data ``plt.plot`` call
    **kwargs:
        Additional arguments that are passed to both ``plt.plot`` calls

    Returns
    -------
    ax : plt.Axes
        The Matplotlib axes.

    Example
    -------
    .. jupyter-execute::

        import matplotlib.pyplot as plt
        import numpy as np
        import pyobsmod.plots as pp

        obs = np.sin(np.arange(100)) + np.random.normal(size=100)
        pred = np.sin(np.arange(100)) + np.random.normal(size=100)
        ax = pp.time_series_plot(
                obs, pred, stats_list=['bias', 'rmse', 'nrmse', 'r2']
            )
        plt.show()
    """
    __checkarrays([obs, pred])

    op = Dataset(obs, pred, time=time)
    ax = op.time_series_plot(
        stats_list=stats_list,
        stats_labels=stats_labels,
        ax=ax,
        textbox_kws=textbox_kws,
        obs_kws=obs_kws,
        pred_kws=pred_kws,
        **kwargs
    )

    return ax


def __checkarrays(arrs: np.ndarray) -> None:
    for arr in arrs:
        if not isinstance(arr, np.ndarray):
            try:
                arr = np.array(arr)
            except ValueError:
                raise ValueError(
                    "Data can not be converted to numpy array. Check data type"
                )
        if arr.squeeze().ndim > 1:
            raise ValueError("Array is not 1-d, verify the dimensions.")
