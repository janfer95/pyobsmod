"""Fundamental framework for the systematic manipulation and visualization of
observed and modelled data."""

__version__ = "0.1.3"

from .dataset import Dataset  # noqa
from .dataset import load_dataset  # noqa
