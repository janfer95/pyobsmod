=======
History
=======

********
Releases
********

0.1.3 (2024-01-08)
~~~~~~~~~~~~~~~~~~

New features
------------
* Dataset and plot methods/functions have now an ax argument that can be used to pass already existing axes (created with for example plt.subplots).

Documentation changes
---------------------
* Add the corresponding documentation.

Credits
-------
Kuhn, Jannik.

0.1.2 (2023-12-19)
~~~~~~~~~~~~~~~~~~

New features
------------
* Add saving and loading capabilities to the Dataset object.
* Add stand-alone plots in a specific module.

Documentation changes
---------------------
* Add documentation plots tutorial.

Credits
-------
Kuhn, Jannik.

0.1.1 (2023-12-18)
~~~~~~~~~~~~~~~~~~

Documentation changes
---------------------
* First official documentation hosted on read the docs.

Credits
-------
Danglade, Nikola.

0.1.0 (2023-12-14)
~~~~~~~~~~~~~~~~~~

New features
------------
* First official realease of `pycoastalwater`.

Credits
-------
Danglade, Nikola & Kuhn, Jannik.