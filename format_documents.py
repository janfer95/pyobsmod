"""Module to reformat python scripts based on python conventions."""

import os

os.system('black pyobsmod/__init__.py')
os.system('black pyobsmod/dataset.py')
os.system('black tests/test_dataset.py')

os.system('docformatter --in-place pyobsmod/__init__.py')
os.system('docformatter --in-place pyobsmod/dataset.py')
os.system('docformatter --in-place tests/test_dataset.py')