"""The setup information necessary for installing the package using pip."""

from setuptools import setup

if __name__ == "__main__":
    setup()
