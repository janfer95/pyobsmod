API documentation
=================

This reference manual provides detailed information about the functions, 
modules, and objects included in `pyobsmod`. It describes their 
purpose, functionality, and how they can be used.

.. currentmodule:: pyobsmod

pyobsmod
--------

.. autosummary::
   :nosignatures:
   :toctree: generated/

   Dataset
   load_dataset

pyobsmod.Dataset
----------------

Utility methods for quick reading and writing of data

.. autosummary::
   :nosignatures:
   :toctree: generated/

   Dataset.save

The methods encapsulated within a Dataset object facilitate the computation of 
statistical data.

.. autosummary::
   :nosignatures:
   :toctree: generated/

   Dataset.bias
   Dataset.lr
   Dataset.nrmse
   Dataset.r
   Dataset.r2
   Dataset.compute_stats
   Dataset.describe_dataset

The methods encapsulated within a Dataset object facilitate the visualization of 
the data.

.. autosummary::
   :nosignatures:
   :toctree: generated/

   Dataset.scatter_plot
   Dataset.scatter_plot_sns
   Dataset.time_series_plot

pyobsmod.plots
--------------

Functions that facilitate the visualization of scatter data.

.. autosummary::
   :nosignatures:
   :toctree: generated/

   plots.scatter_plot
   plots.scatter_plot_sns
   plots.time_series_plot