pyobsmod
========

.. image:: _static/logo.png
   :width: 75

`pyobsmod` stands as a fundamental framework for the systematic manipulation and 
visualization of observed and modelled data.

.. toctree::
   :hidden:
   :caption: Contents:

   getting-started-guide/index.rst
   tutorials/index.rst


.. toctree::
   :hidden:
   :caption: Help & References:

   api/index.rst
   developer.rst
   history.rst
   kostarisk.rst
