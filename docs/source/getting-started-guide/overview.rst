Overview
========

What is pyobsmod ?
------------------

`pyobsmod` is a python package  designed for handling and visualizing observed and 
modelled data. It likely provides a set of tools and functions to manipulate, analyze, 
and visualize datasets related to observations and model outputs. 