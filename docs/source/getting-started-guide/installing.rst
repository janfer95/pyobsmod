Installation
============

To install `pyobsmod`, you can use `pip`.

1. Ensure you have python and git install.

2. Open a terminal and run the following command to install `pyobsmod`:

   .. code-block:: bash

      pip install git+https://gitlab.com/kostarisk/pyobsmod
