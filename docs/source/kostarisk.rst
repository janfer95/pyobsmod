Kostarisk
=========

**Cross-Border Laboratory for Coastal Risk Research**

.. image:: _static/kostarisk.png
   :width: 150

Scientific director : Denis Morichon

AZTI partner manager : Julien Mader

SUEZ RPT partner manager : Matthias Delpey

The LabCom KOSTA RISK (Kosta = Coast in Basque) is a project for a joint cross-border laboratory for applied research in observation and modelling
to support coastal risk management. It associates the Wave Interaction and Structure team of the SIAME laboratory, the Spanish technological center
AZTI and the monitoring and forecasting center Rivages Pro Tech (RPT) of the SUEZ group, relying on a strong complementarity of their respective
expertise, built on the basis of a collaboration of more than 15 years.

.. image:: _static/azti.png
   :width: 200

.. image:: _static/e2s.png
   :width: 200

.. image:: _static/rpt.png
   :width: 200

The creation of this laboratory is part of a context of increased exposure of coastlines to coastal risks such as marine submersion and erosion,
intensified by the effects of global warming. Coastal risk management has now become a major issue for coastal communities and more broadly for
public authorities, an issue to which KOSTA RISK wishes to bring new answers through an original partnership and innovative approaches.

The objective of this laboratory is to bring together researchers from the three organizations in order to implement scientific and technological
cooperation in the fields of numerical modeling, physical measurement systems and advanced data analysis, for the development of tools to assist in
the management and mitigation of coastal risks. The LabCom team is composed of 5 teacher-researchers, 11 partner researchers (6 for RPT and 5 for
AZTI), and may occasionally rely on the technical staff of the partners. It benefits from the financial support of the partners and the E2S consortium.