pyobsmod dataset
================

Import required packages.

.. jupyter-execute::

    import matplotlib.pyplot as plt
    import numpy as np
    import pyobsmod as pom

Create dataset
--------------

Create a dataset from some data.

.. jupyter-execute::

    obs = np.sin(np.arange(100)) + np.random.normal(size=100)
    pred = np.sin(np.arange(100)) + np.random.normal(size=100)
    op = pom.Dataset(obs, pred)
    print(op)

Manipulate dataset
------------------

It is possible to access one time step or slice it in Python fashion.

.. jupyter-execute::

    print(op[10])
    print(op[-10:])

Or get the length of the observations.

.. jupyter-execute::

    print(len(op))

You can also access the data directly. Either one by one:

.. jupyter-execute::

    print(op.obs)

or all the data together (``op.data`` does work as well):

.. jupyter-execute::

    print(op.values)

You can also access the DataFrame that the class is built on.

.. jupyter-execute::

    print(op.df)

If you want to perform actions on the DataFrame itself, for example, taking
the mean of each column.

.. jupyter-execute::

    print(op.df.mean())

Or adding a new column.

.. jupyter-execute::

    op.df['new_column'] = np.random.random(100)

    print(op)

Statistic mehods
----------------

Try out some of the statistic methods.

.. jupyter-execute::

    print(op.compute_stats(['bias', 'rmse']))

Get a quick description of your dataset

.. jupyter-execute::

    print(op.describe_dataset())

Plotting methods
----------------

Or plot your data with matplotlib or seaborn.

.. jupyter-execute::

    grid = op.scatter_plot_sns(['bias', 'rmse', 'nrmse', 'r2'])
    fig = grid.fig
    ax = grid.ax_joint
    plt.show()

It is also possible to use the plots only as a subplot. Create
a figure and at least one axes instance and simply pass the axis
to the method!

.. jupyter-execute::

    fig, axs = plt.subplots(3, 1, figsize=(20, 10))
    # First subplot with pyobsmod
    op.time_series_plot(ax=axs[0])
    # Second subplot with pyobsmod
    op.scatter_plot(['bias', 'rmse', 'nrmse', 'r2'], ax=axs[1])
    # Third subplot whatever you want to plot
    axs[2].plot(obs, obs)
    plt.show()

Note however, that this does NOT work with the ``Dataset.scatter_plot_sns``
function due to the way how seaborn handles this particular plot.

Save/Load dataset
-----------------

Once you are done with working on your data, you can save it.

.. jupyter-execute::
    
    op.save("my_dataset")

The next time you can load your data simply with:

.. jupyter-execute::

    op = pom.load_dataset("my_dataset")