pyobsmod plots
==============

If you don't necessarily want to work with a class instance, then you can
use the ``pyobsmod.plots`` module to plot your data directly. Here are some
examples. All of the examples provided here work also with the
``pyobsmod.Dataset`` class directly, try it out!

First, import required packages.

.. jupyter-execute::

    import matplotlib.pyplot as plt
    import numpy as np
    import pyobsmod.plots as pp

Create some random data.

.. jupyter-execute::


    obs = np.sin(np.arange(100)) + np.random.normal(size=100)
    pred = np.sin(np.arange(100)) + np.random.normal(size=100)
    time = np.arange(50, 150)

Let's take a look at the different plotting options. The first one is a
simple scatter plot with the reference line already plotted for you. If
needed, you can also add some additional statistics, by passing a list of
valid names to the ``stats_list`` argument. 

.. jupyter-execute::

    ax = pp.scatter_plot(obs, pred, stats_list=["rmse", "r2"])
    plt.show()

You can also give alternative names to the statistical quantities. Just pass
``stats_label`` as well. Make sure that they are in the same order.

.. jupyter-execute::

    ax = pp.scatter_plot(
        obs,
        pred,
        stats_list=["rmse", "r2"],
        stats_labels=["RMSE [m]", "Correlation"],
    )

If you want to modify the identity line or the textbox you can pass all the
typical matplotlib keywords to the function in the form of a dictionary.
For example for a dashed identity line and a larger fontsize of the textbox
that we want to put in the right bottom corner of the plot:

.. jupyter-execute::

    ax = pp.scatter_plot(
        obs,
        pred,
        ["rmse", "r2"],
        stats_labels=["RMSE [m]", "Correlation"],
        idline_kws=dict(ls="--"),
        textbox_kws=dict(loc="lower right",
        prop=dict(size=18)),
    )
    plt.show()

Continuing like this, you can refine your plot as you please.
There is another scatter plot function, which is very similar, but uses
seaborn's ``seaborn.jointplot`` as a basis. This gives you marginal plots
and a regression estimate (with standard deviations) in addition to the
identity line and the scatter plot of your data set. As before you can tweak
the plot as you like by passing keyword dictionaries, for this take a look
at the seaborn web page. Here is the simplest example:

.. jupyter-execute::

    grid = pp.scatter_plot_sns(obs, pred)
    plt.show()

If you are interested in the evolution of your time series over time, you
can use the ``pyobsmod.plots.time_series_plot`` function. As before you
can compute and plot some statistical quantities along with it and if you 
want you can also pass a time array, which will be used as the x-axis. 

.. jupyter-execute::

    ax = pp.time_series_plot(obs, pred, time)
    plt.show()

You can also use these plots as subplots in a larger figure. You simply
can pass the axis to the function, for more information scroll through the
:doc:`pyobsmod Dataset tutorial<pyobsmod-dataset>`.

Take a look at our :doc:`../api/index` to dig deeper
and look at the matplotlib and seaborn documentations to find out more about
the keywords to tune your plots accordingly!
