"""Test the pyobsmod.dataset module."""

import numpy as np
import pyobsmod


def test_dataset():
    obs = np.random.random(100)
    pred = np.random.random(100)
    op = pyobsmod.Dataset(obs, pred)

    stats = op.compute_stats(["bias", "lr", "rmse", "nrmse", "r", "r2"])
